﻿namespace UCFramework.API
{
    // Абстрактное событие на передачу абстрактного контекста
    public interface IPluginContext
    {
        string doSomething { get; set; }
    }
}