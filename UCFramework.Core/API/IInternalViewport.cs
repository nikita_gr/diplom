﻿using System.Windows.Forms;

namespace UCFramework.API
{
    // Форма, реализующая данный интерфейс, получит возможность
    // регистрироваться как дочерняя форма, иметь иконку на панели запущеных окон
    // главной формы приложения и т.д.
    public interface IInternalViewport
    {
        // Вернуть форму, на которую будут распространяться функионал
        // MDI дочернего окна, согласно API ядра
        Form AsForm();
        // Вернуть свойства окна, некие параметры для инициализации формы
        FormProperties Properties();
    }
}