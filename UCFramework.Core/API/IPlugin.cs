﻿using System.Collections.Generic;
using UCFramework.Model;

namespace UCFramework.API
{
    // Минимальная реализация данного интерфейса определяетя ядром 
    // как плагин
    public interface IPlugin
    {
        // Возвращает информацию о плагине
        PluginProperties GetPluginProperties { get; }
        // Событие, которое происходит при активации плагина
        void PerformAction(IPluginContext context);
        // Регистрирует плагин в необходимых пунктах меню главного приложения
        IList<PluginMenuItem> RegisterMenuItems();
        // Событие возникает сразу после загрузки сборки в CLI среду
        void InitializationPhase(UCController controller);
    }
}