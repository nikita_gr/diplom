﻿using System.Collections.Generic;
using System.Reflection;
using UCFramework.Model;

namespace UCFramework.API
{
    // Абстрактный класс плагина/модуля
    // Концепция:
    // Любую задачу можно представить как автономный процесс, существующий и действующий 
    // по определенным правилам. Наличие той или иной функции отдельного плагина влияет 
    // на возможности приложения в целом. 
    // Так используя некий набор API который предоставляет система/ядро,
    // можно пополнять функциональность приложения с минимальными внедрениями зависимостей
    public abstract class AbstractPlugin : IPlugin
    {
        // Свойство плагина
        private readonly PluginProperties _properties = new PluginProperties();

        // Ссылка на главный контроллер приложения
        protected UCController controller;

        // Конструктор абстрактного плагина
        protected AbstractPlugin()
        {
            // Запоминаем в свойствах плагина информацию о сборке, которой принадлежит плагин
            _properties.AssemblyInformation = Assembly.GetAssembly(GetType()).GetName();
            // Пополняем свойства плагина из конечной реализации класса
            _properties = PresetProperties(_properties);
        }

        // Абстрактное объявление метода
        protected abstract PluginProperties PresetProperties(PluginProperties properties);

        // По требованию возвращаем свойства плагина
        public PluginProperties GetPluginProperties
        {
            get { return _properties; }
        }

        // ФАза инициализации плагина, можно расширять для дополнения функционала
        public void InitializationPhase(UCController controller)
        {
            //А пока, только получаем, и сохраняем ссылку на главный контроллер приложения
            this.controller = controller;
        }

        // Абстрактное упрощенное событие/действие выполняемое плагином
        public abstract void PerformAction(IPluginContext context);
        // Абстрактное событие, при регистрации плагина в пунктах меню главного приложения
        public abstract IList<PluginMenuItem> RegisterMenuItems();
    }
}