﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using UCFramework.API;
using UCFramework.Controls;
using UCFramework.Model;

namespace UCFramework
{
    // Контроллер управления формами приложения, плагинов/модулей
    public class UCFormController
    {
        //Ссылка на контроллер
        private readonly UCController _controller;
        //Список открытых окон
        private readonly IList<MDIComponent> _formList = new List<MDIComponent>();
        //Список пунктов главного меню
        private IList<ToolStripMenuItem> _menuItemList;
        // Список групп для панели открытых окон
        private readonly GroupBox _group = new GroupBox();

        public UCFormController(UCController controller)
        {
            _controller = controller;
        }

        // Метод открывает дочернее MDI окно
        public void ChildFormOpen(IInternalViewport viewport)
        {
            // Получаем ссылку на главную форму приложения
            var mdiParent = _controller.GeneralViewport.AsForm();

            // Получаем ссылку на панель открытых окон
            var navigationBar = _controller.GeneralViewport.GetNavigationBar();
            // Получаем ссылку на открываемое окно
            var form = viewport.AsForm();
            // Смена режима формы
            form.MdiParent = mdiParent;
            form.Dock = DockStyle.Fill;
            form.Show();
            form.WindowState = FormWindowState.Maximized;

            // СОздаем кнопку
            var button = new FormControlButton(viewport);
            // Кнопку помещаем в группу
            _group.Controls.Add(button);
            //и располагаем на панели открытых окон
            navigationBar.Controls.Add(button);
            // имитируем нажатие на кнопке
            button.PerformClick();
            // Вешаем слушатель на закрытие формы
            form.FormClosing += ChildFormClosingEvent;
            // Вешаем слушатель на активацию формы
            form.Activated += ChildFormActivateEvent;
            // Помещаем форму в список открытых окон
            _formList.Add(new MDIComponent(viewport, button));
        }

        // Событие активации формы
        private void ChildFormActivateEvent(object sender, EventArgs e)
        {
            foreach (var component in _formList)
            {
                if (component.Viewport != null)
                {
                    if (component.Viewport.AsForm() == (Form) sender)
                    {
                        //Console.WriteLine("Активация формы " + sender);
                        // Делаем кнопку, соответствующую активированной форме, нажатой
                        component.Button.Checked = true;
                        break;
                    }
                }
            }
        }

        // Событие закрытия формы
        private void ChildFormClosingEvent(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (var component in _formList)
            {
                if (component.Viewport != null)
                {
                    if (component.Viewport.AsForm() == (Form) sender)
                    {
                        // Удаляем кнопку соответствующую закрываемой форме с панели запущенных окон
                        _controller.GeneralViewport.GetNavigationBar().Controls.Remove(component.Button);
                        // Удаляем форму из списка запущенных окон 
                        _formList.Remove(component);
                        break;
                    }
                }
            }
            // ПРобегаем по списку оставшихся открытых окон и делаем нажатой последнюю
            var c = _formList.Count;
            try
            {
                if (c > 0)
                {
                    _formList[c - 1].Button.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        // Метод подготовки главного меню приложения
        public void PrepareMenu()
        {
            // Получаем ссылку на главное меню приложения
            var menu = _controller.GeneralViewport.GetMenuStrip();
            // Получаем все пункты меню в единый список, для регистрации плагинов вменю
            _menuItemList = GetItems(menu);
        }

        // Возвращает пункты главного меню приложения  в виде списка
        public List<ToolStripMenuItem> GetItems(MenuStrip menuStrip)
        {
            // инициализация списка
            var myItems = new List<ToolStripMenuItem>();
            // В цикле берем пункты верхнего уровня
            foreach (ToolStripMenuItem i in menuStrip.Items)
            {
                GetMenuItems(i, myItems);
            }
            return myItems;
        }
        // ПОлучаем подпункты меню для меню верхнего уровня рекурсивно
        private void GetMenuItems(ToolStripMenuItem item, List<ToolStripMenuItem> items)
        {
            items.Add(item);
            foreach (ToolStripItem i in item.DropDownItems)
            {
                if (i is ToolStripMenuItem)
                {
                    GetMenuItems((ToolStripMenuItem) i, items);
                }
            }
        }

        // Речистрация пунктов меню плагинов в главном меню приложения
        public void RegisteredPluginMenuItems(IList<PluginMenuItem> pluginMenu)
        {
            if (pluginMenu != null)
            {
                Console.Write(@"Регистрация в главном меню...");
                // Цикл по меню плагина, если установлены
                foreach (var item in pluginMenu)
                {
                    if (item != null)
                    {
                        if (item.MenuItem != null && item.DestinationPath != null)
                        {
                            // Ищем необходимый пункт меню
                            foreach (var menuIitem in _menuItemList)
                            {
                                
                                if (menuIitem.Text.ToLower().Equals(item.DestinationPath.ToLower()))
                                {
                                    //и добавляем меню плагина в пункт назначения
                                    menuIitem.DropDownItems.Add(item.MenuItem);
                                    Console.Write(@"[{0}]", item.MenuItem);
                                }
                            }
                        }
                    }
                }
                Console.Write("\n");
            }
        }
    }
}