﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UCFramework.API;
using UCFramework.Utils;

namespace UCFramework
{
    // Контроллер плагин-системы, ищет сборки с характерной 
    // "подписью" (Реализация интерфейса IPlugin)
    // Загружает в массивы, получает необходимую информацию после создания экземпляра
    // И выполняет все необходимые мероприятия по инициализации
    internal sealed class UCPluginController
    {
        // Список плагинов
        private readonly ICollection<IPlugin> _plugins;
        // Ссылка на контроллер
        private readonly UCController _controller;


        public UCPluginController(UCController controller)
        {
            _controller = controller;
            _plugins = loadPlugin();
        }

        // Вывод  в консоль данных о загруженных плагинах
        public void PluginsReview()
        {
            foreach (var plugin in _plugins)
            {
                Console.WriteLine(PluginDump(plugin));
            }
        }

        // Вывод информации о плагине на консоль
        public string PluginDump(IPlugin plugin)
        {
            string result = null;
            if (plugin != null)
            {
                var properties = plugin.GetPluginProperties;
                if (properties != null)
                {
                    result += "************************************************************************************\n";
                    result += string.Format("PluginName: [{0}], Plugin Type [{1}]\n", properties.Name, properties.Type);
                    result += string.Format("Description: [{0}]\n", properties.Description);

                    var assemblyName = properties.AssemblyInformation;
                    if (assemblyName != null)
                    {
                        result += string.Format("Library Name: [{0}], Library Version [{1}]", assemblyName.Name,
                            assemblyName.Version);
                    }
                }
            }
            return result;
        }

        // Инициализация плагинов
        public void PreparePlugins(UCFormController formController)
        {
            // Просмотрим есть ли регистрация плагина в пунктах меню
            foreach (var plugin in _plugins)
            {
                var pluginProperties = plugin.GetPluginProperties;

                Console.WriteLine(PluginDump(plugin));

                var pluginMenu = plugin.RegisterMenuItems();
                if (pluginMenu != null)
                {
                    // Регистрация плагина в главном меню
                    formController.RegisteredPluginMenuItems(pluginMenu);
                }
            }
        }

        public void PreparePluginsDAOLayer(UCFormController formController)
        {
            // Просмотрим есть ли регистрация плагина в пунктах меню
            foreach (var plugin in _plugins)
            {
                /*   var pluginDAO = (IRegisterDAO)plugin;

                if (pluginDAO is IRegisterDAO)
                {

                    Console.WriteLine("********************************************************");
                    Console.WriteLine(pluginDAO.GetDAOTypes);
                }
              * */
            }
        }

        // Загрузка плагинов в систему
        private ICollection<IPlugin> loadPlugin()
        {
            string[] dllFileNames = {};
            // Обзорная директория
            var path = FileUtils.GetDirectoryForFile(Assembly.GetEntryAssembly().Location);//+ @"/plugins" ;
            Console.WriteLine("Plugin dir path..." + path);
            if (Directory.Exists(path))
            {
                dllFileNames = Directory.GetFiles(path, "*.dll");
            }

            IList<string> fileList = new List<string>();
            // ПОлучаем список всех dll в папке [plugins]
            foreach (var fileName in dllFileNames)
            {
                fileList.Add(fileName);
            }
            // Получаем служебную информацию о dll
            ICollection<Assembly> assemblies = new List<Assembly>(fileList.Count);
            assemblies.Add(Assembly.GetEntryAssembly());

            foreach (var dllFile in fileList)
            {
                try
                {
                    var an = AssemblyName.GetAssemblyName(dllFile);
                    var assembly = Assembly.Load(an);
                    assemblies.Add(assembly);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Не корректная сборка");
                }
            }

            // Есть ли в сборке файлы, реализующие IPlugin
            var pluginType = typeof (IPlugin);
            ICollection<Type> pluginTypes = new List<Type>();
            foreach (var assembly in assemblies)
            {
                if (assembly != null)
                {
                    try
                    {
                        var types = assembly.GetTypes();
                        foreach (var type in types)
                        {
                            if (type.IsInterface || type.IsAbstract)
                            {
                            }
                            else
                            {
                                // И если есть, то добавляем в список плагингов
                                if (type.GetInterface(pluginType.FullName) != null)
                                {
                                    pluginTypes.Add(type);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {

                    }

                }
            }

            // Инстаниируем плагины, проводим первичную инициализацию
            ICollection<IPlugin> plugins = new List<IPlugin>(pluginTypes.Count);
            foreach (var type in pluginTypes)
            {
                // Новый экземпляр плагина
                var plugin = (IPlugin) Activator.CreateInstance(type);
                // Первичная инициализация плагина
                plugin.InitializationPhase(_controller);
                // Добавляем плагин в окончательный список плагинов
                plugins.Add(plugin);
            }

            return plugins;
        }
    }
}