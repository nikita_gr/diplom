﻿using System;
using UCFramework.Utils;

namespace UCFramework.Model

{
    [Serializable]
    // Информация о приложении
    public class ApplicationInformation
    {
        //Имя приложения
        public string Name { get; set; }
        // Путь запуска приложения
        public string ProgramRunPath { get; set; }
        //Аргументы командной строки
        public string[] Arguments { get; set; }
        //Конструктор
        public ApplicationInformation(string name, string applicationFile, string[] arguments)
        {
            Name = name;
            ProgramRunPath = FileUtils.GetDirectoryForFile(applicationFile);
            Arguments = arguments;
        }
    }
}