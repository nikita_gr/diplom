﻿using System.Drawing;

namespace UCFramework.API
{
    // Свойства формы
    public class FormProperties
    {
        // Имя формы, текст в заголовке
        public string Name { get; set; }
        // Имя пиктограммы для формы, картинка должна находиться в папке [img]
        public string IconFileName { get; set; }
        // Размер формы, если форма имеет тип, отличный от ChildForm
        public Size? Size { get; set; }


        public FormProperties()
        {

        }

        public FormProperties(string iconFileName, Size size)
        {
            IconFileName = iconFileName;
            Size = size;
        }
    }
}