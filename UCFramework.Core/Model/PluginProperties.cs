﻿using System.Reflection;
using UCFramework.API;

namespace UCFramework.Model
{
    // Свойство плагина
    public sealed class PluginProperties
    {
        //Имя сборки, системная информация
        public AssemblyName AssemblyInformation { get; set; }
        //Имя плагина
        public string Name { get; set; }
        //Описание плагина
        public string Description { get; set; }
        // Тип плагина
        public PluginType Type { get; set; }
    }
}