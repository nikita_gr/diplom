﻿using System.Drawing;
using System.Windows.Forms;
using UCFramework.API;

namespace UCFramework.Model
{
    // Концепция:
    // Базовый функционал IInternalViewport приходится реализовывать в каждой новой форме, 
    // и дабы упростить, ускорить разработку и разделить представление от контроллера в MVC
    // паттерне UI/
    // Абстрактный режим скрывает всю обязательную реализацию от программиста непосредственно режима
    // а форму режима выводит в разряд дополнительного свойства.
    public abstract class AbstractFunctionalMode : IInternalViewport
    {
        //Ссылка на форму
        protected Form Viewport;
        //Ссылка на контроллер
        protected UCController Controller;
        //Свойства режима
        private FormProperties _properties;
        // Тип области просмотра/формы
        protected FormViewportType ViewportType;
        //Имя функционального режима, будет помещено в заголовок формы
        protected string ModeName { get; set; }

        //Конструктор. Если тип вьюпорта не указан, по-умолчанию будет дочерним MDI окном
        protected AbstractFunctionalMode(UCController controller,
            FormViewportType viewportType = FormViewportType.ChildForm)
        {
            ViewportType = viewportType;
            Controller = controller;
        }

        //Инициализация режима
        protected void InitializeFunctionalMode()
        {
            //Подготовка формы для функционального режима
            PrepareViewport();
            //Установка свойств режима
            _properties = SetProperties(new FormProperties());
            if (Viewport != null)
            {
                if (_properties != null)
                {
                    //Если свойства режима установлены/дополнены, форма будет проинициализирована ими
                    Viewport.Text = _properties.Name;
                    if (ViewportType != FormViewportType.ChildForm)
                    {
                        if (_properties.Size != null)
                        {
                            //Размер формы согласно свойств
                            Viewport.Size = (Size) _properties.Size;
                            Viewport.StartPosition = FormStartPosition.CenterScreen;
                        }
                    }
                }
            }
        }

        // Отображаем форму функционального режима исходя из типа вьюпорта
        protected void Show()
        {
            switch (ViewportType)
            {
                case FormViewportType.NormalForm:
                    Viewport.Show();
                    break;

                case FormViewportType.ChildForm:
                    Controller.OpenChildFrame(this);
                    break;

                case FormViewportType.DialogForm:
                    Viewport.ShowDialog();
                    break;
            }
        }
        // Инициализирует область просмотра
        protected abstract void PrepareViewport();
        // Возвращает ссылку на форму
        public Form AsForm()
        {
            return Viewport;
        }
        // Возвращает свойства формы
        public FormProperties Properties()
        {
            return _properties;
        }
        //Устанавливает и/или перезаписывает свойства формы
        public abstract FormProperties SetProperties(FormProperties properties);
    }
}