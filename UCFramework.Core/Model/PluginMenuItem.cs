﻿using System.Windows.Forms;

namespace UCFramework.Model
{
    // Структура для регистрации плагина в пунктах меню главного приложения
    public class PluginMenuItem
    {
        // Имя пункта меню, куда будет добавлен MenuItem
        public string DestinationPath { get; set; }
        // Пункт меню, который будет добавлен в главное меню приложения
        public ToolStripMenuItem MenuItem { get; set; }

        public PluginMenuItem(string destinationPath, ToolStripMenuItem menuItem)
        {
            DestinationPath = destinationPath;
            MenuItem = menuItem;
        }
    }
}