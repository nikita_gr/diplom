﻿using UCFramework.API;
using UCFramework.Controls;

namespace UCFramework.Model
{
    // MDI компонент, хранит ссылку на MDI дочернюю форму и соответствующую ей кнопку
    // на панели запущеных окон
    public class MDIComponent
    {
        //Ссылка на форму
        public IInternalViewport Viewport { get; private set; }
        //Ссфлка на кнопку
        public FormControlButton Button { get; private set; }

        public MDIComponent(IInternalViewport viewport, FormControlButton button)
        {
            Viewport = viewport;
            Button = button;
        }
    }
}