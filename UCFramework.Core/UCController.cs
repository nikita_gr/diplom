﻿using System;
using System.Windows.Forms;
using UCFramework.API;
using UCFramework.Configuration;
using UCFramework.DAO.Repository;
using UCFramework.Model;

namespace UCFramework
{
    // Контроллер "ядра" приложения
    public class UCController
    {
        // Информация о приложении
        private readonly ApplicationInformation _information;
        // Ссылка на контроллер окон
        private readonly UCFormController _formController;
        // Контроллер плагин системы
        private readonly UCPluginController _pluginController;
        // Контроллер управления конфигурацией
        private readonly UCConfiguration _configuration;
        // Ссылка на главную форму приложения
        private IGeneralViewport _generalViewport;
        // Ссылка на обработчик всплывающих подсказок
        public static ToolTip ToolTipHandler = new ToolTip();
        // Метод установки главного окна приложения
        public IGeneralViewport GeneralViewport
        {
            get { return _generalViewport; }
            set
            {
                _generalViewport = value;
                Console.WriteLine(@"Инициализация главной формы приложения...");
            }
        }

        public UCController(ApplicationInformation information)
        {
            _information = information;
            _formController = new UCFormController(this);
            _pluginController = new UCPluginController(this);
            _configuration = new UCConfiguration(this);
        }

        // Метод открывает дочернюю форму
        public void OpenChildFrame(IInternalViewport viewport)
        {
            _formController.ChildFormOpen(viewport);
            Console.WriteLine(@"Открытие формы {0} ...", viewport.AsForm().Text);
        }
        // Метод инициализации плагинов
        public void PreparePlugins()
        {
            Console.WriteLine(@"Инициализация плагинов...");
            _pluginController.PreparePlugins(_formController);
        }
        // Метод инициализации пользовательского интерфейса
        public void PrepareUI()
        {
            Console.WriteLine(@"Инициализация главного меню...");
            _formController.PrepareMenu();
        }

        //Возвращает интерфейс для доступа к конфигурации приложения
        public IConfiguration GetConfiguration()
        {
            if (_configuration != null)
            {
                // Инициализируем, если не была проинициализирована
                _configuration.PrepareConfiguration();
                return _configuration;
            }
            return null;
        }
        // Возвращает интерфейс для доступа к конфигурации плагинов
        public IPluginConfiguration GetPluginConfiguration(string pluginName)
        {
            if (_configuration != null)
            {
                // Инициализируем, если не была проинициализирована
                _configuration.PrepareConfiguration(pluginName);
                return _configuration;
            }
            return null;
        }

        // Регистрация DQO провайдера
        public bool RegisterAsDAOProvider(IDAOProvider provider)
        {
            if (provider != null)
            {
                var list = provider.GetDAOProviders();
                if (list.Count > 0)
                {
                    //DAOFactory проинициализирована списком подключений
                    var factory = DAOFactory.GetInstance();
                    factory.SetProviders(list);
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}