﻿using System;
using System.Drawing;
using System.Windows.Forms;
using UCFramework.API;

namespace UCFramework.Controls
{
    // Компонент кнопка, соответствующая дочерней MDI форме, открытой  d ukfdyjv ghbkj;tybb
    public partial class FormControlButton : RadioButton
    {
        // Ссылка на Форму/область просмотра
        public IInternalViewport Viewport { get; private set; }

        public FormControlButton(IInternalViewport viewport)
        {
            InitializeComponent();
            //Установка декорации и поведение компонента
            FlatAppearance.BorderColor = Color.Gray;
            FlatAppearance.CheckedBackColor = Color.FromArgb(215, 215, 215);
            FlatStyle = FlatStyle.Flat;
            TextImageRelation = TextImageRelation.ImageBeforeText;
            ImageAlign = ContentAlignment.MiddleLeft;
            // Привязка кнопки к форме
            Viewport = viewport;

            // Попытка получить и установить пиктограмму для кнопки, соответствующей открываемой форме
            // Имя картинки получаем из свойств формы, установленых пользователем в процессе инициализации
            if (Viewport != null)
            {
                if (Viewport.Properties() != null)
                {
                    try
                    {
                        Image = Image.FromFile(@"img\" + Viewport.Properties().IconFileName);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
            // Привязка события клика на кнопке
            CheckedChanged += ButtonClickShowFormEvent;
            // Получаем текст формы и помещаем на кнопку
            var caption = viewport.AsForm().Text;
            Appearance = Appearance.Button;
            UseCompatibleTextRendering = true;
            Margin = new Padding(0, 1, 1, 2);

            // Обрезаем длинное название ограничивая 50-ю символами
            // В подсказку при наведении на кнопку помещаем полное название формы
            var maxlength = 50;
            var tooLongCaption = "";
            if (caption.Length <= maxlength)
            {
                maxlength = caption.Length;
            }
            else
            {
                tooLongCaption = "...";
            }

            Text = caption.Substring(0, maxlength) + tooLongCaption;
            // Регистрируем подсказку
            UCController.ToolTipHandler.SetToolTip(this, caption);
        }

        // Событие при клике мыши на кнопке
        private void ButtonClickShowFormEvent(object sender, EventArgs e)
        {
            var button = sender as FormControlButton;
            if (button != null)
                // Кнопка эта с состоянием поэтому реагируем только на нажатую 
                if (button.Checked)
                {
                    // Через вьюпорт получаем ссылку на форму
                    var form = button.Viewport.AsForm();
                    if (form != null)
                    {
                        // И если форма получена, перемещаем ее на передний план
                        form.BringToFront();
                    }
                }
        }
    }
}