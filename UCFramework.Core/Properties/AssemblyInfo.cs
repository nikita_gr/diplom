﻿
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information
[assembly: AssemblyTitle("UCFramework: Core")]
[assembly: AssemblyDescription("Useful Component Framework Core")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("AndyFry")]
[assembly: AssemblyProduct("UCFramework: Core")]
[assembly: AssemblyCopyright("AndyFreeDev@gmail.com, Гомель 2018г.")]
[assembly: AssemblyTrademark("AndyFreeDev@gmail.com, Гомель 2018г.")]
[assembly: AssemblyCulture("")]

// Version informationr(
[assembly: AssemblyVersion("0.0.0.422")]
[assembly: AssemblyFileVersion("0.0.0.422")]
[assembly: NeutralResourcesLanguageAttribute( "en-US" )]

