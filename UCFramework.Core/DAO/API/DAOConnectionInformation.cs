﻿namespace UCFramework.DAO.API
{
    // Сведения о подключении к БД
    // В случае использования БД требующих дополнительные параметры, например
    // имя пользователя/пароль/url/SSL и т.д то функционал расширяется/дополняется
    public class DAOConnectionInformation
    {
        //Строка подключения к БД
        public string ConnectionString { get; set; }
        //Имя файла БД
        public string Database { get; set; }

        // Имя пользователя
        // Пароль
        // ConnectionPoolProvider
        // SyntaxProvider
        //.........
    }
}