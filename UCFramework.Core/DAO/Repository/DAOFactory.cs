﻿using System.Collections.Generic;
using UCFramework.DAO.API;

namespace UCFramework.DAO.Repository
{
    // Базовая реализация паттерна фабрика/синглтон
    // В случае данного приложения содержит список провайдеров СУБД
    public class DAOFactory
    {
        //Ссылка инстанса, в единичном экземпляре
        public static DAOFactory _instance;
        // Список провайдеров
        private List<DAOConnectionInformation> list;

        // Тут что-то выполняем при инициализации
        private DAOFactory()
        {
        }

        // Главный метод вызова DAO
        public static DAOFactory GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DAOFactory();
            }
            return _instance;
        }

        // Возвращает данные о провайдере СУБД по индексу
        public DAOConnectionInformation GetConnectionByIndex(int index)
        {
            if (list != null)
            {
                return list[index];
            }
            return null;
        }

        // Инициализация DAO списком провайдеров при первичной инициализации приложения 
        public void SetProviders(List<DAOConnectionInformation> daoConnection)
        {
            list = daoConnection;
        }
    }
}