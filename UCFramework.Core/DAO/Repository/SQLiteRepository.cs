﻿using System;
using Mono.Data.Sqlite;

namespace UCFramework.DAO.Repository
{
    // SQL репозиторий СУБД SQLite, по-необходимости пополняется
    // Стандартная и общеизвестная реализация методом
    public class SQLiteRepository
    {
        // Создание файла БД
        public static void CreateDatabse(string dbName)
        {
            SqliteConnection.CreateFile(dbName);
        }

        // Выполнение SQL запроса без результата
        public static void ExecuteNonQuery(string connectionString, string sqlStatement)
        {
           var dbConnection = new SqliteConnection(connectionString);
          /*  */ dbConnection.Open();
          var command = new SqliteCommand(sqlStatement, dbConnection);
            command.ExecuteNonQuery();
            dbConnection.Close();
            
        }
        // Выполнение SQL запроса с обратным вызовом
        public void Execute(string connectionString, Func<SqliteConnection, SqliteCommand> callback)
        {
            var dbConnection = new SqliteConnection(connectionString);
            dbConnection.Open();
            var command = callback.Invoke(dbConnection);
            command.ExecuteNonQuery();
            dbConnection.Close();
        }
    }
}