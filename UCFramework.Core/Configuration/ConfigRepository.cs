﻿using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UCFramework.DAO.Repository;

namespace UCFramework.Configuration
{
    // Репозиторий DAO для работы с конфигурацией приложения и плагинов
    public sealed class ConfigRepository
    {
        // Создание таблицы в БД хранения конфигурационных параметров
        private const string sqlCreateTable =
    @"CREATE TABLE IF NOT EXISTS @TABLENAME (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, KEY TEXT(255), VALUE TEXT(255) )";
        // Выбрать параметр из БД по ключу
        private const string sqlSelect = "SELECT * FROM @TABLENAME WHERE KEY=@KEY";
        // Вставить параметр в БД
        private const string sqlInsert = "INSERT INTO @TABLENAME (KEY, VALUE) VALUES (@KEY, @VALUE)";
        // Обновить параметр в БД по ключу
        private const string sqlUpdate = "UPDATE @TABLENAME SET VALUE=@VALUE WHERE KEY=@KEY";

        // Имя файла БД для хранения конфигурации
        public const string CONFIG_DB = "config.sqlite";
        // Имя таблицы в БД конфигурации
        public const string TABLE_MAIN_SETTING = "main_settings";
        // Строка подключения к БД конфигурации
        public const string ConnectionString = "Data Source=" + CONFIG_DB + ";Version=3;";

        // Возвращает Параметр конфигурации из необходимой таблицы по ключу
        public string GetPropertyValueByKey(string tableName, string key)
        {
            // Если имя таблицы или ключ не инициализированы - выходим
            if (tableName == null && key == null)
            {
                return null;
            }
            // Подготавливает запрос для работы с определенной таблицей
            var sql = sqlSelect.Replace("@TABLENAME", tableName);
            
            //Инициализируем список результата запроса
            var result = new List<string>();
            // Стандартная констукция для запроса к БД
            using (var connect = new SqliteConnection(ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    // Заполнение запроса параметрами
                    command.CommandText = sql;
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@KEY", key);
                    // Выполнение запроса
                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        // ПОлучаем результат и помещаем его в список результата
                        result.Add((string) dataReader["VALUE"]);
                    }
                }
            }
            // Если параметр по ключу не обнаружен - возвращает null иначе 0 элемент списка
            return result.Count > 0 ? result[0] : null;
        }

        // Записываем параметр в БД
        public void SetPropertyValueByKey(string tableName, string key, string value)
        {
            // Если имя таблицы или ключ не инициализированы - выходим
            if (tableName == null && key == null)
            {
                return;
            }
            //Проверяем, есть ли такой параметр в БД и выбираем соответствующий запрос
            // на добавление или обновление записи
            var s = GetPropertyValueByKey(tableName, key);
            var sql = s == null ? sqlInsert : sqlUpdate;
            //Стандартная конструкция выполнения SQL запроса в БД
            using (var connect = new SqliteConnection(ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    // Заполнение запроса параметрами
                    command.CommandText = sql.Replace("@TABLENAME", tableName);
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@KEY", key);
                    command.Parameters.AddWithValue("@VALUE", value);
                    command.ExecuteNonQuery();
                }
                connect.Close();
            }
        }

        // Создание конфигурационного файла БД
        public void CreateConfigurationFile()
        {
            // Обращаемся к главному DAO репозиторию SQLite
            SQLiteRepository.CreateDatabse(CONFIG_DB);
        }

        // Создание необходимой таблицы если она не обнаружена в БД
        // Например добавлен новый плагин, который сохраняет некие параметры в БД,
        // а соответствующей таблицы не обнаружено
        public static void PrepareConfiguration(string tableName)
        {
            // Обращаемся к главному DAO репозиторию SQLite
            SQLiteRepository.ExecuteNonQuery(ConnectionString, sqlCreateTable.Replace("@TABLENAME", tableName));
        }
    }
}