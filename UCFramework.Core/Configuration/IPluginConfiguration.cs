﻿namespace UCFramework.Configuration
{
    // Интерфейс предоставляет доступ к изменяемым параметрам палгина
    // которые храняться в БД
    public interface IPluginConfiguration
    {
        // Получить параметр конфигурации по ключу из БД для плагина
        string GetPluginProperty(string pluginName, string key, string defValue = null);
        // Записать параметр в БД для плагина
        void SetPluginProperty(string pluginName, string key, string value);
    }
}