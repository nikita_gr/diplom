﻿using System.IO;

namespace UCFramework.Configuration
{
    // Объект управления конфигурацией
    // Имеет методы предоставления доступа к параметрам приложения и плагинов
    public class UCConfiguration : IConfiguration, IPluginConfiguration
    {
        // Репозиторий DAO конфигурации
        private readonly ConfigRepository _repository;

        // Конструктор конфигуратора
        public UCConfiguration(UCController controller)
        {
            //Инициализация репозитория
            _repository = new ConfigRepository();

            //Если файл БД конфигурации не найден - созадем
            if (!File.Exists(ConfigRepository.CONFIG_DB))
            {
                _repository.CreateConfigurationFile();
            }
        }

        // ПОлучение параметра из БД
        private string GetProperty(string tableName, string key, string defValue = null)
        {
            // Получем параметр из репозитория
            var result = _repository.GetPropertyValueByKey(tableName, key);
            // Если параметр не найден
            if (result == null)
            {
                //И если значение по умолчанию определено
                if (defValue != null)
                {
                    //Запишем параметр в БД и веренем значение по-умолчанию
                    SetApplicationProperty(key, defValue);
                    return defValue;
                }
            }
            return result;
        }

        // Запись параметра в БД
        private void SetProperty(string tableName, string key, string value)
        {
            //Через репозиторий
            _repository.SetPropertyValueByKey(tableName, key, value);
        }

        // Возвращает параметр по ключу, либо значение по-умолчанию если определено
        public string GetApplicationProperty(string key, string defValue = null)
        {
            return GetProperty(ConfigRepository.TABLE_MAIN_SETTING, key, defValue);
        }

        // Запись параметра приложения в БД
        public void SetApplicationProperty(string key, string value)
        {
            SetProperty(ConfigRepository.TABLE_MAIN_SETTING, key, value);
        }

        // Возвращает параметр по ключу, либо значение по-умолчанию если определено
        public string GetPluginProperty(string pluginName, string key, string defValue = null)
        {
            return GetProperty(pluginName, key, defValue);
        }

        // Запись параметра плагина в БД
        public void SetPluginProperty(string pluginName, string key, string value)
        {
            SetProperty(pluginName, key, value);
        }

        // Подготовка конфигурации, инициализация таблиц
        public void PrepareConfiguration(string pluginName = null)
        {
            // Если имя плагина не определено, принимаем, что обращаемся к конфигурации приложения
            if (pluginName == null)
            {
                pluginName = ConfigRepository.TABLE_MAIN_SETTING;
            }
            // Подготавливаем таблицу к работе
            ConfigRepository.PrepareConfiguration(pluginName);
        }
    }
}