﻿namespace UCFramework.Configuration
{
    // Интерфейс предоставляет доступ к изменяемым параметрам приложения
    // которые храняться в БД
    public interface IConfiguration
    {
        // ПОлучить параметр конфигурации по ключу из БД
        string GetApplicationProperty(string key, string defValue = null);
        // Записать параметр в БД
        void SetApplicationProperty(string key, string value);
    }
}