﻿using System;
using System.IO;

namespace UCFramework.Utils
{
    // Вспомогательный утилитарный класс - сборник
    // распространенных решений тех или иных задач
    public class FileUtils
    {
        // ВОзвращает домашний каталог
        public static string GetHomeDirectory()
        {
            var path =
                Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            if (Environment.OSVersion.Version.Major >= 6)
            {
                path = Directory.GetParent(path).ToString();
            }
            return path;
        }
        // Возвращает дирректорию в которой расположен файл
        public static string GetDirectoryForFile(string filePath)
        {
            if (filePath != null)
            {
                if (File.Exists(filePath))
                {
                    return Path.GetDirectoryName(filePath);
                }
                return null;
            }
            return filePath;
        }

        // Создание дирректории
        public static bool CreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
                return false;
            }
            return true;
        }
    }
}