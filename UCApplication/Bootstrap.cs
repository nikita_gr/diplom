﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using UCFramework;
using UCFramework.API;
using UCFramework.DAO.API;
using UCFramework.Model;
using UCTestApplication;

namespace UCApplication
{
    // Загрузчик приложения
    // Введен, как "прослойка" выполняющая некоторые действия
    // при инициализации приложения
    internal class Bootstrap:IDAOProvider
    {
        public Bootstrap(string[] args)
        {
            // Информация о приложении: Имя, путь запуска, аргументы командной сторки
            var information = new ApplicationInformation("UCTestApplication", GetRunPath(), args);
            // Инициализация "ядра"
            var controller = new UCController(information);
            //Иниицализация главной формы приложения
            var form = new MainForm(controller);
            // Инициализация модуля хранения конфигурации
            var config = controller.GetConfiguration();
            // Тестовое получение данных некоторого параметра
            var value = config.GetApplicationProperty("main-test-key", "test-value");
            // Вывод параметра в консоль
            Console.WriteLine("Параметр для ключа " + value);

            // Регистрация загрузчика в качестве DAO провайдера
            controller.RegisterAsDAOProvider(this);
            // Подготовка пользовательского интерфейса
            controller.PrepareUI();
            // Подготовка плагинов и модулей к загрузке
            controller.PreparePlugins();
            // Активация приложения
            Application.Run(form);
        }

        // Реализация метода интерфеса IDAOProvider
        // В данном методе формируется абстрактный список доступных подключений к БД
        // Так как в нашем приложении в качестве СУБД используется SQLite
        // то структура данных подключений минимальна и ограничена
        // именем файла БД и строки подключения ADO.Net
        public List<DAOConnectionInformation> GetDAOProviders()
        {
            // Инициализация списка подключений
            var daoProviders = new List<DAOConnectionInformation>();
            // Объект описания подключения
            var connection = new DAOConnectionInformation();
            // Имя файла БД
            connection.Database = "currency.sqlite";
            // Строка подключения к СУБД
            connection.ConnectionString = "Data Source=" + connection.Database + ";Version=3;";
            // Добавляем объект подключения в список подключений
            daoProviders.Add(connection);

            // ВОзвращаем для регистрации вкачестве DAO поставщика, абстрактно
            return daoProviders;
        }
        
        //Вспомогательный метод, возвращает путь запуска приложения
        private static string GetRunPath()
        {
            return Assembly.GetEntryAssembly().Location;
        }
    }
}