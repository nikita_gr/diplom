﻿using System.Drawing;
using System.Windows.Forms;
using UCFramework.API;

namespace UCApplication.Forms
{
    // Тестовая дочерняя форма, открывается внутри MDI приложения
    public partial class ChildForm : Form, IInternalViewport
    {
        // Экземпляр свойств формы,  
        // содержит информацию первичной инизиализации формы
        private FormProperties _properties;

        public ChildForm(string text)
        {
            InitializeComponent();
            Text = text;
        }

        // Реализация метода интерфеса IInternalViewport
        public Form AsForm()
        {
            //Возвращает ссылку на форму
            return this;
        }

        // Реализация метода интерфеса IInternalViewport
        public FormProperties Properties()
        {
            //Инициализирует, если не проинициализированы
            //а затем возвращает свойства формы
            return _properties ?? (_properties = new FormProperties("update_24.png", new Size(800, 600)));
        }
    }
}