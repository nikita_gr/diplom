﻿using System;
using System.Windows.Forms;
using UCApplication.Forms;
using UCApplication.Modes;
using UCFramework;
using UCFramework.API;

namespace UCTestApplication
{
    // Главная форма приложения, настроена в соответствии с MDI паттерном
    // Является parent формой для дочерних форм
    // Конструктивно имеет компоненты Главное меню, Панель инструментов, 
    // Панель открытых форм, Панель состояния 
    public partial class MainForm : Form, IGeneralViewport
    {
        // Ссылка на главный контроллер фреймворка
        private readonly UCController _controller;
        // Конструктор формы
        public MainForm(UCController controller)
        {
            // Сохраняем ссылку на главный контроллер
            _controller = controller;
            //Инициализация компонентов формы
            InitializeComponent();
            // Определяем данную форму как главная форма приложения
            controller.GeneralViewport = this;
        }

        // Событие клик меню ВЫХОД
        private void miExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        // Запуск тестового функционального режима, как обычной формы
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            new TestFunctionalMode(_controller, FormViewportType.NormalForm);
        }

        // Запуск тестового функционального режима, как дочерней формы MDI
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            new TestFunctionalMode(_controller);
        }

        // Реализация метода интерфеса IGeneralViewport
        public Form AsForm()
        {
            return this;
        }

        // Реализация метода интерфеса IGeneralViewport
        public FlowLayoutPanel GetNavigationBar()
        {
            return pWindowList;
        }

        // Реализация метода интерфеса IGeneralViewport
        public MenuStrip GetMenuStrip()
        {
            return menuStrip;
        }

        private void tsAbout_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.ShowDialog();
        }


    }
}