﻿using System;
using System.Windows.Forms;

//using Newtonsoft.Json ;

namespace UCApplication
{
    // Стандартный раннер приложения
    internal static class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // Инициализация загрузчика
            new Bootstrap(args);
        }
    }
}