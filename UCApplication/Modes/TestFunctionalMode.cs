﻿using System.Drawing;
using UCFramework;
using UCFramework.API;
using UCFramework.Model;

namespace UCApplication.Modes
{
    // Тестовый функциональный режим, подробности в описании AbstractFunctionalMode
    public class TestFunctionalMode : AbstractFunctionalMode
    {
        // Конструктор режима
        public TestFunctionalMode(UCController controller, FormViewportType viewportType = FormViewportType.ChildForm)
            : base(controller, viewportType)
        {
            InitializeFunctionalMode();
            Show();
        }

        // Реализация метода AbstractFunctionalMode
        protected override void PrepareViewport()
        {
            // В качестве главной формы данного режима возвращается некая форма
            Viewport = new TestFunctionalModeForm();
        }

        // Реализация метода AbstractFunctionalMode
        public override FormProperties SetProperties(FormProperties properties)
        {
            // Свойство режима/формы -
            // Имя, наименование
            properties.Name = "Тестовый функциональный режим";
            // Пиктограмма для данного, будет отображаться на панели открытых форм
            properties.IconFileName = "car_24.png";
            // Размер главной формы функционального режима
            properties.Size = new Size(800, 600);
            return properties;
        }
    }
}