﻿
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information
[assembly: AssemblyTitle("UCFramework: Test Unit")]
[assembly: AssemblyDescription("Useful Component Framework Test Unit")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("AndyFry")]
[assembly: AssemblyProduct("UCFramework: Test Unit")]
[assembly: AssemblyCopyright("AndyFreeDev@gmail.com, Гомель 2018г.")]
[assembly: AssemblyTrademark("AndyFreeDev@gmail.com, Гомель 2018г.")]
[assembly: AssemblyCulture("")]

// Version informationr(
[assembly: AssemblyVersion("0.0.0.556")]
[assembly: AssemblyFileVersion("0.0.0.556")]
[assembly: NeutralResourcesLanguageAttribute( "en-US" )]

