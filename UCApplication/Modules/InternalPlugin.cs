﻿using System;
using System.Collections.Generic;
using UCFramework.API;
using UCFramework.Model;

namespace UCApplication.Modules
{
    // Класс тестового плагина
    // В концепции, возможно рассматривать модули главного приложения как внешний плагин.
    // Использовать API фреймворка абстрагируясь от кода главного приложения
    // Загрузчик/Контроллер плагинов, предполагается, различает как плагины основной сборки(приложения)
    // так и внешние сборки, загружаемые из каталога [plugins]
    // Подробнее в описании класса AbstractPlugin
    public class InternalPlugin : AbstractPlugin
    {
        // Реализация метода класса AbstractPlugin/IPlugin
        protected override PluginProperties PresetProperties(PluginProperties properties)
        {
            // Тип плагина - Служба
            properties.Type = PluginType.Service;
            // Название плагина
            properties.Name = "Internal plugin module";
            // Описание плагина
            properties.Description = "Внутренний плагин приложения";

            return properties;
        }

        // Реализация метода класса AbstractPlugin/IPlugin
        public override void PerformAction(IPluginContext context)
        {
            throw new NotImplementedException();
        }

        // Реализация метода класса AbstractPlugin/IPlugin
        public override IList<PluginMenuItem> RegisterMenuItems()
        {
            return null;
        }
    }
}