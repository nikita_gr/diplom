﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UCFramework.API;
using UCFramework.Model;
using UCPlugin.Forms;

namespace UCPlugin
{
    public class CurrencyMonitorPlugin : AbstractPlugin
    {
        public override void PerformAction(IPluginContext context)
        {
            context.doSomething =
                ParseEmail(context.doSomething);
        }

        // Регистрация пунктов меню плагина
        public override IList<PluginMenuItem> RegisterMenuItems()
        {
            // Список меню к регистрации
            IList<PluginMenuItem> menuList = new List<PluginMenuItem>(); 
            // Главная ветка
            ToolStripMenuItem mainMenu = new ToolStripMenuItem("Monitor");
            // Пункт монитора
            ToolStripMenuItem smCurrGetter = new ToolStripMenuItem("Монитор курсов валюты");
            // Как выпадающий пункт
            mainMenu.DropDownItems.Add(smCurrGetter);
            // Событие при клике на пункте
            smCurrGetter.Click += smCurrGetter_Click;
            // Добавляем в список
            menuList.Add(new PluginMenuItem("Дополнения", mainMenu));
            return menuList;
        }

        void smCurrGetter_Click(object sender, EventArgs e)
        {
            // При клике на пункте меню, запускаем режим монитора
            new CurrencyRateGetterFunctionalMode(controller);
        }

        // Тестовый метод парцит текст, находит email адреса, и возвращает
        private string ParseEmail(string text)
        {
            const string emailPattern = @"\w+@\w+\.\w+((\.\w+)*)?";
            var emails =
                Regex.Matches(text, emailPattern,
                    RegexOptions.IgnoreCase);
            var emailString = new StringBuilder();
            foreach (Match email in emails)
            {
                emailString.Append(email.Value + Environment.NewLine);
            }

            return emailString.ToString();
        }

        // Свойства плагина
        protected override PluginProperties PresetProperties(PluginProperties properties)
        {
            properties.Type = PluginType.Common;
            properties.Name = "Currency monitor";
            properties.Description = "Монитор курсов валюты по нац. банку";

            return properties;
        }
    }
}