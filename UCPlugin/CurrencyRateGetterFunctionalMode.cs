﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Xml.Linq;
using UCFramework;
using UCFramework.API;
using UCFramework.DAO.Repository;
using UCFramework.Model;
using UCPlugin.DAO;
using UCPlugin.Forms;
using UCPlugin.Model;

namespace UCPlugin
{
    // Функциональный режим Мониторинга курсов валюты
    public class CurrencyRateGetterFunctionalMode : AbstractFunctionalMode
    {
        // Репозиторий DAO 
        private CurrencyRepository _repository;
        // ФОрма монитора курсов валюты
        private CurrGetterForm _viewport;
        // Рабочий список курсов валюты, быдет отображаться в гриде
        private List<ViewCurrencyRate> rateList;


        public CurrencyRateGetterFunctionalMode(UCController controller,
            FormViewportType viewportType = FormViewportType.ChildForm)
            : base(controller, viewportType)
        {
            InitializeFunctionalMode();
            // Подготовка БД
            PrepareDatabase();
            // Отвязка от событий из нескольких потоков UI
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            // События для фонового процессора
            _viewport.backgroundWorker.DoWork += backgroundWorker_DoWork;
            _viewport.backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;
            // Событие при клике на кнопке
            _viewport.btnGetRates.Click += btnGetRates_Click;
            // Событие при изменении даты в календаре
            _viewport.dateTimePicker.ValueChanged += dateTimePicker_ValueChanged;
            // Запуск фонового процессора
            _viewport.backgroundWorker.RunWorkerAsync();

            Show();
        }


        // Метод подготовки БД
        private void PrepareDatabase()
        {
            // Получаем доступ к провайдеру БД
            var factory = DAOFactory.GetInstance();
            // Инициализируем репозиторий курсов валюты
            _repository = new CurrencyRepository(factory.GetConnectionByIndex(0));
            // Создание БД курсов валюты
            _repository.CreateDatabaseFile();
        }

        // Инициализация формы режима
        protected override void PrepareViewport()
        {
            _viewport = new CurrGetterForm();
            Viewport = _viewport;
        }

        // Предустановка свойств фуцнкционального режима
        public override FormProperties SetProperties(FormProperties properties)
        {
            properties.Name = "Мониторинг курсов иностранной валюты";
            properties.IconFileName = "car_24.png";
            properties.Size = new Size(800, 600);
            return properties;
        }


        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            _viewport.backgroundWorker.RunWorkerAsync();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            rateList = _repository.GetCurrencyRatesByDate(_viewport.dateTimePicker.Value.ToString("MM.dd.yyyy"));
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _viewport.dataGridView.DataSource = rateList;
        }

        // Клик по кнопке получить курс
        private void btnGetRates_Click(object sender, EventArgs ev)
        {
            // Имя файла в котором будем хранить курсы
            var currencyFileName = "CurrencyRate.xml";
            //ФОрмат даты для запроса к службе Нац.Банка
            var dateRequest = _viewport.dateTimePicker.Value.ToString("MM/dd/yyyy");

            var bgThread = new Thread(() =>
            {
                using (var client = new WebClient())
                {
                    // Подлучаем курс валюты от сервиса Нац. банка
                    var url = @"http://www.nbrb.by/Services/XmlExRates.aspx?ondate=" + dateRequest;
                    File.WriteAllText(currencyFileName, client.DownloadString(url));
                }

                // Парсинг файла курсов валюты
                var rates = new CurrencyRates();
                var doc = XDocument.Load(currencyFileName);

                if (doc.Root != null)
                {
                    var date = (string) doc.Root.Attribute("Date");
                    // Пишем в структуры и пополняем список курсов
                    var result = doc.Descendants("Currency")
                        .Select(e => new Currency
                        {
                            Id = (int) e.Attribute("Id"),
                            NumCode = (string) e.Element("NumCode"),
                            CharCode = (string) e.Element("CharCode"),
                            Scale = (int) e.Element("Scale"),
                            Name = (string) e.Element("Name"),
                            Rate = (double) e.Element("Rate")
                        })
                        .ToList();

                    rates.SetRateList(result);
                    rates.Date = date;
                }
                // Отправляем в репозиторий список валют
                // Для сохранения наименований валюты
                _repository.AddCurrencyRates(rates);
                //Для сохранения курсов валюты 
                _repository.AddCurrencyNames(rates);
                // Обновляем содержимое грида
                _viewport.backgroundWorker.RunWorkerAsync();
            });

            bgThread.Start();
        }
    }
}