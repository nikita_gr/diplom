﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using UCFramework.DAO.API;
using UCFramework.DAO.Repository;
using UCPlugin.Model;

namespace UCPlugin.DAO
{
    // Репозиторий ДЛя работы с курсами валюты
    public class CurrencyRepository
    {
        private DAOConnectionInformation _connectInfo;
        // SQL запрос создания таблицы наименования иностранных валют
        private const string sqlCreateCurrencyNameTable =
            @"CREATE TABLE IF NOT EXISTS CURRENCY_NAMES (
            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            CODE INTEGER NOT NULL, 
            CHAR_CODE TEXT(3), 
            NAME TEXT(100), 
            SCALE REAL)";
        // SQL запрос создания таблицы курсов валюты
        private const string sqlCreateCurrencyRatesTable =
            @"CREATE TABLE IF NOT EXISTS CURRENCY_RATES (
            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            DATE TEXT (10), 
            CURR_CODE INTEGER,            
            RATE REAL)";
        // SQL запрос получения наименований валюты
        private const string sqlExistCurrencyName = "SELECT * FROM CURRENCY_NAMES WHERE CODE=@CODE";
        // SQL запрос получения курса валюты
        private const string sqlExistCurrencyRate = "SELECT * FROM CURRENCY_RATES WHERE CURR_CODE=@CURR_CODE AND DATE=@DATE";
        // SQL запрос добавления наименования валюты
        private const string sqlInsertCurrencyNames =
            "INSERT INTO CURRENCY_NAMES ('CODE', 'CHAR_CODE', 'NAME', 'SCALE') VALUES (@CODE, @CHAR_CODE, @NAME, @SCALE);";
        // SQL запрос добавления курса валюты
        private const string sqlInsertCurrencyRates =
            "INSERT INTO CURRENCY_RATES ('DATE', 'CURR_CODE', 'RATE') VALUES (@DATE, @CURR_CODE, @RATE);";
        // SQL запрос обновление курса валюты
        private const string sqlUpdateCurrencyRates =
            "UPDATE CURRENCY_RATES SET RATE=@RATE WHERE DATE=@DATE AND CURR_CODE=@CURR_CODE";
        // SQL запрос получения курсов валюты на определенную дату
        private const string sqlSelectRatesByDate = "SELECT * FROM VCurrencyRates WHERE DATE=@DATE";

        public CurrencyRepository(DAOConnectionInformation connection)
        {
            _connectInfo = connection;
        }

        // Создания файла базы данных
        public void CreateDatabaseFile()
        {
            //если файл БД не обнаружен
            if (!File.Exists(_connectInfo.Database))
            {
                // Создаем файл БД
                SQLiteRepository.CreateDatabse(_connectInfo.Database);
            }
            // Создаем необходимые таблицы
            SQLiteRepository.ExecuteNonQuery(_connectInfo.ConnectionString, sqlCreateCurrencyNameTable);
            SQLiteRepository.ExecuteNonQuery(_connectInfo.ConnectionString, sqlCreateCurrencyRatesTable);
        }

        // Добавляем курсы валюты в БД
        public void AddCurrencyRates(CurrencyRates rates)
        {
            // получаем доступные валюты
            var list = rates.GetRateList();
            // Подготавливаем дату для хранения в БД
            string date = rates.Date.Replace("/", ".");

            // Конструкция подключения к БД
            using (var connect = new SqliteConnection(_connectInfo.ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    // Выполняем действия в пределах транзакции
                    using (var transaction = connect.BeginTransaction())
                    {
                        foreach (var currency in list)
                        {
                            // Если курс отсутствует в базе
                            if (!ExistCurrencyRate(date, currency.Id))
                            {
                                // принимаем запрос на добавление
                                command.CommandText = sqlInsertCurrencyRates;
                            }
                            else
                            {
                                // принимаем запрос на изменение
                                command.CommandText = sqlUpdateCurrencyRates;
                            }
                            //@DATE, @CURR_CODE, @RATE
                            command.Parameters.AddWithValue("@DATE", date);
                            command.Parameters.AddWithValue("@CURR_CODE", currency.Id);
                            command.Parameters.AddWithValue("@RATE", currency.Rate);

                            command.ExecuteNonQuery();
                        }
                        // Коммит транзакции
                        transaction.Commit();
                        command.Dispose();
                    }
                }
                connect.Close();
            }
        }

        // Добавляем в базу наименования валют
        public void AddCurrencyNames(CurrencyRates rates)
        {
            // получаем доступные валюты
            var list = rates.GetRateList();
            // Конструкция подключения к БД
            using (var connect = new SqliteConnection(_connectInfo.ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    // Выполняем действия в пределах транзакции
                    using (var transaction = connect.BeginTransaction())
                    {
                        foreach (var currency in list)
                        {
                            // ПРоверка, есть ли такая валюта в базе
                            if (!ExistCurrency(currency.Id))
                            {
                                //Пишем в БД только валюту, код которой нет в базе
                                Console.WriteLine(currency.ToString());
                                command.CommandText = sqlInsertCurrencyNames;
                                //CODE, CHAR_CODE, NAME, SCALE
                                command.Parameters.AddWithValue("@CODE", currency.Id);
                                command.Parameters.AddWithValue("@CHAR_CODE", currency.CharCode);
                                command.Parameters.AddWithValue("@NAME", currency.Name);
                                command.Parameters.AddWithValue("@SCALE", currency.Scale);
                                command.ExecuteNonQuery();
                            }
                        }
                        // Коммит транзакции
                        transaction.Commit();
                        command.Dispose();
                    }
                }
                connect.Close();
            }
        }

        // Метод проверяет, есть ли код валюты в БД
        private bool ExistCurrency(int code)
        {
            using (var connect = new SqliteConnection(_connectInfo.ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    command.CommandText = sqlExistCurrencyName;
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@CODE", code);
                    var result = command.ExecuteScalar();
                    if (result != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        // Проверяет, есть ли курс валюты на указанную дату в БД
        private bool ExistCurrencyRate(string date, int code)
        {
            using (var connect = new SqliteConnection(_connectInfo.ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    command.CommandText = sqlExistCurrencyRate;
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@CURR_CODE", code);
                    command.Parameters.AddWithValue("@DATE", date);
                    var result = command.ExecuteScalar();
                    if (result != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        // Метод возвращает курсы валют на указанную дату, 
        // помещает в список в виде объектов
        public List<ViewCurrencyRate> GetCurrencyRatesByDate(string date)
        {
            List<ViewCurrencyRate> result = new List<ViewCurrencyRate>();


            using (var connect = new SqliteConnection(_connectInfo.ConnectionString))
            {
                connect.Open();
                using (var command = connect.CreateCommand())
                {
                    command.CommandText = sqlSelectRatesByDate;
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@DATE", date);
                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        ViewCurrencyRate item = new ViewCurrencyRate();
                        
                        item.Id = (long) dataReader["ID"];
                        item.Date = (string) dataReader["DATE"];
                        item.Name = (string)dataReader["NAME"];
                        item.CharCode = (string)dataReader["CHAR_CODE"];
                        item.Scale = (long)dataReader["SCALE"];
                        item.Code = (long)dataReader["CURR_CODE"];
                        item.Rate = ((float)dataReader["RATE"]).ToString("0.0000");
                        
                        result.Add(item);
                    }
                }
                connect.Close();
            }
            return result;
        } 
    }
}