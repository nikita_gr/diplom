﻿
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information
[assembly: AssemblyTitle("UCFramework: General Plugin")]
[assembly: AssemblyDescription("Useful Component Framework Plugin")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("AndyFry")]
[assembly: AssemblyProduct("UCFramework: General Plugin")]
[assembly: AssemblyCopyright("AndyFreeDev@gmail.com, Гомель 2018г.")]
[assembly: AssemblyTrademark("AndyFreeDev@gmail.com, Гомель 2018г.")]
[assembly: AssemblyCulture("")]

// Version informationr(
[assembly: AssemblyVersion("0.0.0.109")]
[assembly: AssemblyFileVersion("0.0.0.109")]
[assembly: NeutralResourcesLanguageAttribute( "en-US" )]

