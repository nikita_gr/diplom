﻿namespace UCPlugin.Model
{
    public class ViewCurrencyRate
    {
        public long Id { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public string CharCode { get; set; }
        public long Code { get; set; }
        public long Scale { get; set; }
        public string Rate { get; set; }
    }
}