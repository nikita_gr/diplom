﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCPlugin.Model
{
   public class CurrencyRates
    {
        public string Date { get; set; }
        private List<Currency> _rates;

        public void SetRateList(List<Currency> list)
        {
            _rates = list;
        }

        public List<Currency> GetRateList()
        {
            return _rates;
        } 
    }
}
