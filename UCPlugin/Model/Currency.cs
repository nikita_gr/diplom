﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCPlugin.Model
{
    public class Currency
    {
        public int Id { get; set; }
        public string NumCode { get; set; }
        public string CharCode { get; set; }
        public int Scale { get; set; }
        public string Name { get; set; }
        public double Rate { get; set; }

        public override string ToString()
        {
            return Id+" - "+NumCode+" - "+CharCode+" - "+Scale+" - "+Name+" - "+Rate;
        }


    }
}
